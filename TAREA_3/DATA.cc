#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>

using namespace RooFit;
using namespace RooStats;

double efi(double x, double y)
{
double r2=x/y;
return r2;
}

double eefi(double x, double y)
{
double efi=x/y;
double r2=sqrt(efi*(1-efi)/y);
return r2;
}

void DATA() 
{

TFile *file = TFile::Open("../Data_8TeV.root"); 
TTree *tree = (TTree*) file->Get("mini");
//tree->Print();
TFile *file1 = TFile::Open("../ttbar_8TeV.root"); 
TTree *tree1 = (TTree*) file1->Get("mini");

//----------------------------------------------------------------------------------
Bool_t e_trig;
Bool_t mu_trig;
Bool_t good_vtx;
UInt_t lep_n;
UInt_t jet_n;
Float_t MET;
Float_t MET_phi;
//------------
Float_t lep_pt[10];  
Float_t lep_eta[10];  
Float_t lep_phi[10];  
Float_t lep_E[10];  
Int_t lep_type[10];  
Float_t lep_ptcone30[10];
Float_t lep_etcone20[10];
//------------
Float_t jet_pt[10];  
Float_t jet_eta[10];  
Float_t jet_jvf[10];  
Float_t jet_MV1[10];  
//------------------------------
Bool_t e_trig_mc;
Bool_t mu_trig_mc;
Bool_t good_vtx_mc;
UInt_t lep_n_mc;
UInt_t jet_n_mc;
Float_t MET_mc;
Float_t MET_phi_mc;
//------------
Float_t lep_pt_mc[10];  
Float_t lep_eta_mc[10];  
Float_t lep_phi_mc[10];  
Float_t lep_E_mc[10];  
Int_t lep_type_mc[10];  
Float_t lep_ptcone30_mc[10];
Float_t lep_etcone20_mc[10];
//------------
Float_t jet_pt_mc[10];  
Float_t jet_eta_mc[10];  
Float_t jet_jvf_mc[10];  
Float_t jet_MV1_mc[10]; 
//------------
Float_t sf_PILEUP;
Float_t sf_ELE;
Float_t sf_MUON;
Float_t sf_BTAG;
Float_t sf_TRIGGER;
Float_t sf_JVFSF;
Float_t sf_ZVERTEX;


//----------------------------------------------------------------------------------
tree->SetBranchAddress("trigE", &e_trig);
tree->SetBranchAddress("trigM", &mu_trig);
tree->SetBranchAddress("hasGoodVertex", &good_vtx);
tree->SetBranchAddress("lep_n", &lep_n);
tree->SetBranchAddress("jet_n", &jet_n);
tree->SetBranchAddress("met_et", &MET);
tree->SetBranchAddress("met_phi", &MET_phi);
//------------
tree->SetBranchAddress("lep_pt", &lep_pt);
tree->SetBranchAddress("lep_eta", &lep_eta);
tree->SetBranchAddress("lep_phi", &lep_phi);
tree->SetBranchAddress("lep_E", &lep_E);
tree->SetBranchAddress("lep_type", &lep_type);
tree->SetBranchAddress("lep_ptcone30", &lep_ptcone30);
tree->SetBranchAddress("lep_etcone20", &lep_etcone20);
//------------
tree->SetBranchAddress("jet_pt", &jet_pt);
tree->SetBranchAddress("jet_eta", &jet_eta);
tree->SetBranchAddress("jet_jvf", &jet_jvf);
tree->SetBranchAddress("jet_MV1", &jet_MV1);
//----------------------------------------
tree1->SetBranchAddress("trigE", &e_trig_mc);
tree1->SetBranchAddress("trigM", &mu_trig_mc);
tree1->SetBranchAddress("hasGoodVertex", &good_vtx_mc);
tree1->SetBranchAddress("lep_n", &lep_n_mc);
tree1->SetBranchAddress("jet_n", &jet_n_mc);
tree1->SetBranchAddress("met_et", &MET_mc);
tree1->SetBranchAddress("met_phi", &MET_phi_mc);
//------------
tree1->SetBranchAddress("lep_pt", &lep_pt_mc);
tree1->SetBranchAddress("lep_eta", &lep_eta_mc);
tree1->SetBranchAddress("lep_phi", &lep_phi_mc);
tree1->SetBranchAddress("lep_E", &lep_E_mc);
tree1->SetBranchAddress("lep_type", &lep_type_mc);
tree1->SetBranchAddress("lep_ptcone30", &lep_ptcone30_mc);
tree1->SetBranchAddress("lep_etcone20", &lep_etcone20_mc);
//------------
tree1->SetBranchAddress("jet_pt", &jet_pt_mc);
tree1->SetBranchAddress("jet_eta", &jet_eta_mc);
tree1->SetBranchAddress("jet_jvf", &jet_jvf_mc);
tree1->SetBranchAddress("jet_MV1", &jet_MV1_mc);
//------------
tree1->SetBranchAddress("scaleFactor_PILEUP", &sf_PILEUP);
tree1->SetBranchAddress("scaleFactor_ELE", &sf_ELE);
tree1->SetBranchAddress("scaleFactor_MUON", &sf_MUON);
tree1->SetBranchAddress("scaleFactor_BTAG", &sf_BTAG);
tree1->SetBranchAddress("scaleFactor_TRIGGER", &sf_TRIGGER);
tree1->SetBranchAddress("scaleFactor_JVFSF", &sf_JVFSF);
tree1->SetBranchAddress("scaleFactor_ZVERTEX", &sf_ZVERTEX);




//----------------------------------------------------------------------------------
TH1F *cutflow = new TH1F("Cutflow","Cutflow; Cut; Events",10,0,10);
//---------------------------
TH1F *lep_pt_wo = new TH1F("lep_pt_wo","lep_pt",100,22000,120000);
TH1F *lep_trackisolation_wo = new TH1F("lep_trackisolation_wo","lep_trackisolation",100,0,0.1);
TH1F *lep_calorimeterisolation_wo = new TH1F("lep_calorimeterisolation_wo","lep_calorimeterisolation",100,0,0.6);
TH1F *lep_eta_wo = new TH1F("lep_eta_wo","lep_eta",100,-3,3);
//--
TH1F *jet_pt_wo  = new TH1F("jet_pt_wo ","jet_pt ",100,20000,150000);
TH1F *jet_eta_wo = new TH1F("jet_eta_wo","jet_eta",100,-3,3);
TH1F *jet_JVF_wo = new TH1F("jet_JVF_wo","jet_JVF",100,-1,1);
TH1F *jet_MV1_wo = new TH1F("jet_MV1_wo","jet_MV1",100,0,1);
//--
TH1F *nbjets_wo  = new TH1F("nbjets_wo","nbjets ",3,0,3);
TH1F *MET_wo = new TH1F("MET_wo","MET",100,0,120000);
TH1F *mtw_wo = new TH1F("mtw_wo","mtw",100,0,150000);

//---------------------------
TH1F *lep_pt_w = new TH1F("lep_pt_w","lep_pt",100,22000,120000);
TH1F *lep_trackisolation_w = new TH1F("lep_trackisolation_w","lep_trackisolation",100,0,0.01);
TH1F *lep_calorimeterisolation_w = new TH1F("lep_calorimeterisolation_w","lep_calorimeterisolation",100,0,0.2);
TH1F *lep_eta_w = new TH1F("lep_eta_w","lep_eta",100,-3,3);
//--
TH1F *jet_pt_w  = new TH1F("jet_pt_w ","jet_pt ",100,20000,150000);
TH1F *jet_eta_w = new TH1F("jet_eta_w","jet_eta",100,-3,3);
TH1F *jet_JVF_w = new TH1F("jet_JVF_w","jet_JVF",100,-1,1);
TH1F *jet_MV1_w = new TH1F("jet_MV1_w","jet_MV1",100,0,1);
//--
TH1F *nbjets_w  = new TH1F("nbjets_w","nbjets ",5,0,5);
TH1F *MET_w = new TH1F("MET_w","MET",100,0,120000);
TH1F *mtw_w = new TH1F("mtw_w","mtw",100,0,150000);
//------------
TH1F *lep_pt_MC = new TH1F("lep_pt_MC","lep_pt",100,22000,120000);
TH1F *lep_trackisolation_MC = new TH1F("lep_trackisolation_MC","lep_trackisolation",100,0,0.01);
TH1F *lep_calorimeterisolation_MC = new TH1F("lep_calorimeterisolation_MC","lep_calorimeterisolation",100,0,0.2);
TH1F *lep_eta_MC = new TH1F("lep_eta_MC","lep_eta",100,-3,3);
//--
TH1F *jet_pt_MC  = new TH1F("jet_pt_MC ","jet_pt ",100,20000,150000);
TH1F *jet_eta_MC = new TH1F("jet_eta_MC","jet_eta",100,-3,3);
TH1F *jet_JVF_MC = new TH1F("jet_JVF_MC","jet_JVF",100,-1,1);
TH1F *jet_MV1_MC = new TH1F("jet_MV1_MC","jet_MV1",100,0,1);
//--
TH1F *nbjets_MC  = new TH1F("nbjets_MC","nbjets ",5,0,5);
TH1F *MET_MC = new TH1F("MET_MC","MET",100,0,120000);
TH1F *mtw_MC = new TH1F("mtw_MC","mtw",100,0,150000);

//----------------------------------------------------------------------------------
int nentries=tree->GetEntries();
std::cout<<nentries<<std::endl;
int cut[9];
cut[0]=nentries;
for(int k=1; k<9; k++){cut[k]=0;}


for (Long64_t ievt=0; ievt<nentries; ievt++)
{

if((ievt%100000)==0){std::cout<<ievt<<std::endl;}
tree->GetEntry(ievt);
//--------------------------------------------------------------
float mTW;
TLorentzVector Lepton; 
TLorentzVector  MeT; 
//----------histogramas sin cortes-------------------------------
MeT.SetPtEtaPhiE(MET,0.0,MET_phi,MET);
for(unsigned int i=0; i<lep_n; i++){
lep_pt_wo->Fill(lep_pt[i]);
lep_trackisolation_wo->Fill(lep_ptcone30[i]/lep_pt[i]);
lep_calorimeterisolation_wo->Fill(lep_etcone20[i]/lep_pt[i]);
lep_eta_wo->Fill(lep_eta[i]);
Lepton.SetPtEtaPhiE(lep_pt[i],lep_eta[i],lep_phi[i],lep_E[i]);
mTW = sqrt(2*Lepton.Pt()*MeT.Et()*(1-cos(Lepton.DeltaPhi(MeT))));
mtw_wo->Fill(mTW);
}
//--
double nbjet=0;
for(unsigned int j=0; j<jet_n; j++)
{
jet_pt_wo ->Fill(jet_pt[j]);
jet_eta_wo->Fill(jet_eta[j]);
jet_JVF_wo->Fill(jet_jvf[j]);
jet_MV1_wo->Fill(jet_MV1[j]);
if(jet_MV1[j]>=0.7892){nbjet++;}
}
//--
nbjets_wo->Fill(nbjet); 
MET_wo->Fill(MET);





	
//-----------------------------------------------------
	//First cut: Good vertex
	if(!good_vtx){continue;}
	cutflow->Fill(1);
	cut[1]++;

//-----------------------------------------------------
	//Second cut: Trigger
	if(!e_trig && !mu_trig){continue;}
	cutflow->Fill(2);
	cut[2]++;

//-----------------------------------------------------
	// Preselection of good leptons                                                                                
	int index=0;
        int type=0;
	int n_lep=0;
	int n_mu=0;
	int n_el=0;

	//Loop over leptons
	for(unsigned int i=0; i<lep_n; i++){
	if( lep_pt[i] < 25000.){continue;}
        if( lep_ptcone30[i]/lep_pt[i] > 0.15 ){continue;}
        if( lep_etcone20[i]/lep_pt[i] > 0.15 ){continue;}
        if( lep_type [i]==13 && TMath::Abs(lep_eta[i]) <2.50 ){n_mu++;}
        if( lep_type [i]==11 && TMath::Abs(lep_eta[i]) <2.47 && (TMath::Abs(lep_eta[i])<1.37 || TMath::Abs(lep_eta[i])>1.52)){n_el++;}
        n_lep=n_el+n_mu;
	if(n_lep==1){index=i; type=lep_type[index];}
        }

	//Select events with only 1 good lepton and fill the cutflow histogram 
	//Example:
	//Third cut (one good lepton):
	if(n_lep!=1){continue;}
	cutflow->Fill(3); 
	cut[3]++;

//-----------------------------------------------------
	int n_jets=0;
	int n_bjets=0;
    
	//Fourth cut: At least 4 jets
	if(jet_n<4){continue;}
	cutflow->Fill(4); 
	cut[4]++;

//-----------------------------------------------------
	for(unsigned int j=0; j<jet_n; j++)
	{
        // To complete: apply jet cuts to find the good jets
        if(jet_pt[j]<25000.){continue;}
        if(TMath::Abs(jet_eta[j])>2.5){continue;}
        if(jet_pt[j]<50000. && TMath::Abs(jet_eta[j])<2.4){if(jet_jvf[j]<0.5){continue;}}
        n_jets++;
        if(jet_MV1[j]<0.7892){continue;}
        n_bjets++;
        }
    
	//Fifth cut: At least 4 good jets
	if(n_jets<4) continue; 
	cutflow->Fill(5); 
	cut[5]++;
    
	//Sixth cut: at least one b-jet
	if(n_bjets<2) continue;
	cutflow->Fill(6); 
	cut[6]++;

//-----------------------------------------------------
        //Seventh cut: MET > 30 GeV
	if(MET<30000.){continue;}
	cutflow->Fill(7); 
	cut[7]++;

//-----------------------------------------------------
	// TLorentzVector definitions

	Lepton.SetPtEtaPhiE(lep_pt[index],lep_eta[index],lep_phi[index],lep_E[index]);
	MeT.SetPtEtaPhiE(MET,0.0,MET_phi,MET);
	//Calculation of the mTW using TLorentz vectors             
	mTW = sqrt(2*Lepton.Pt()*MeT.Et()*(1-cos(Lepton.DeltaPhi(MeT))));
	//Eight cut: mTW > 30 GeV
	if(mTW<30000.){continue;}
	cutflow->Fill(8); 
	cut[8]++;



//----------histogramas con cortes-------------------------------
lep_pt_w->Fill(lep_pt[index]);
lep_trackisolation_w->Fill(lep_ptcone30[index]/lep_pt[index]);
lep_calorimeterisolation_w->Fill(lep_etcone20[index]/lep_pt[index]);
lep_eta_w->Fill(lep_eta[index]);
Lepton.SetPtEtaPhiE(lep_pt[index],lep_eta[index],lep_phi[index],lep_E[index]);
MeT.SetPtEtaPhiE(MET,0.0,MET_phi,MET);
mTW = sqrt(2*Lepton.Pt()*MeT.Et()*(1-cos(Lepton.DeltaPhi(MeT))));
mtw_w->Fill(mTW);

//--
nbjet=0;
for(unsigned int j=0; j<jet_n; j++)
{
jet_pt_w ->Fill(jet_pt[j]);
jet_eta_w->Fill(jet_eta[j]);
jet_JVF_w->Fill(jet_jvf[j]);
jet_MV1_w->Fill(jet_MV1[j]);
if(jet_MV1[j]>=0.7892){nbjet++;}
}
//--
nbjets_w->Fill(nbjet); 
MET_w->Fill(MET);


}



std::cout << "Done!" << std::endl;
std::cout << "All events:" << cut[0] << std::endl;
//for(int l=1; l<9; l++){std::cout << "Cut" <<l<< ": 	" <<cut[l]<<"	"<< efi(cut[l],cut[0])*100 << "%	" << eefi(cut[l],cut[0])*100 << "%	" << efi(cut[l],cut[l-1])*100 << "%	" << eefi(cut[l],cut[l-1])*100<< "%" << std::endl;}
for(int l=1; l<9; l++){std::cout <<cut[l]<<"	"<< efi(cut[l],cut[0])*100 << "%	" << eefi(cut[l],cut[0])*100 << "%	" << efi(cut[l],cut[l-1])*100 << "%	" << eefi(cut[l],cut[l-1])*100<< "%" << std::endl;}

std::cout << "-------------------------------------------------------" << std::endl<< std::endl;

//-------------------------------------------------------------------------------------------------------------------------------


int nentriesMC=tree1->GetEntries();
std::cout<<nentriesMC<<std::endl;


for (Long64_t ievt=0; ievt<nentriesMC; ievt++)
{

if((ievt%100000)==0){std::cout<<ievt<<std::endl;}
tree1->GetEntry(ievt);
	
//-----------------------------------------------------
	//First cut: Good vertex
	if(!good_vtx_mc){continue;}
//-----------------------------------------------------
	//Second cut: Trigger
	if(!e_trig_mc && !mu_trig_mc){continue;}
//-----------------------------------------------------
	// Preselection of good leptons                                                                                
	int index=0;
        int type=0;
	int n_lep=0;
	int n_mu=0;
	int n_el=0;

	//Loop over leptons
	for(unsigned int i=0; i<lep_n_mc; i++){
	if( lep_pt_mc[i] < 25000.){continue;}
        if( lep_ptcone30_mc[i]/lep_pt_mc[i] > 0.15 ){continue;}
        if( lep_etcone20_mc[i]/lep_pt_mc[i] > 0.15 ){continue;}
        if( lep_type_mc [i]==13 && TMath::Abs(lep_eta_mc[i]) <2.50 ){n_mu++;}
        if( lep_type_mc [i]==11 && TMath::Abs(lep_eta_mc[i]) <2.47 && (TMath::Abs(lep_eta_mc[i])<1.37 || TMath::Abs(lep_eta_mc[i])>1.52)){n_el++;}
        n_lep=n_el+n_mu;
	if(n_lep==1){index=i; type=lep_type_mc[index];}
        }

	//Select events with only 1 good lepton and fill the cutflow histogram 
	//Example:
	//Third cut (one good lepton):
	if(n_lep!=1){continue;}


//-----------------------------------------------------
	int n_jets=0;
	int n_bjets=0;
    
	//Fourth cut: At least 4 jets
	if(jet_n_mc<4){continue;}

//-----------------------------------------------------
	for(unsigned int j=0; j<jet_n_mc; j++)
	{
        // To complete: apply jet cuts to find the good jets
        if(jet_pt_mc[j]<25000.){continue;}
        if(TMath::Abs(jet_eta_mc[j])>2.5){continue;}
        if(jet_pt_mc[j]<50000. && TMath::Abs(jet_eta_mc[j])<2.4){if(jet_jvf_mc[j]<0.5){continue;}}
        n_jets++;
        if(jet_MV1_mc[j]<0.7892){continue;}
        n_bjets++;
        }
    
	//Fifth cut: At least 4 good jets
	if(n_jets<4) continue; 
    
	//Sixth cut: at least one b-jet
	if(n_bjets<2) continue;

//-----------------------------------------------------
        //Seventh cut: MET > 30 GeV
	if(MET_mc<30000.){continue;}

//-----------------------------------------------------
	// TLorentzVector definitions
	TLorentzVector Lepton; 
	TLorentzVector  MeT; 
	Lepton.SetPtEtaPhiE(lep_pt_mc[index],lep_eta_mc[index],lep_phi_mc[index],lep_E_mc[index]);
	MeT.SetPtEtaPhiE(MET_mc,0.0,MET_phi_mc,MET_mc);
	//Calculation of the mTW using TLorentz vectors      
	float mTW;       
	mTW = sqrt(2*Lepton.Pt()*MeT.Et()*(1-cos(Lepton.DeltaPhi(MeT))));
	//Eight cut: mTW > 30 GeV
	if(mTW<30000.){continue;}


//----------histogramas con cortes-------------------------------
double wt=(1000*137.29749)/(49761200.21*0.072212854);
double scalefactors=sf_PILEUP*sf_ELE*sf_MUON*sf_BTAG*sf_TRIGGER*sf_JVFSF*sf_ZVERTEX;
double weight=wt*scalefactors;

lep_pt_MC->Fill(lep_pt_mc[index],weight);
lep_trackisolation_MC->Fill(lep_ptcone30_mc[index]/lep_pt_mc[index],weight);
lep_calorimeterisolation_MC->Fill(lep_etcone20_mc[index]/lep_pt_mc[index],weight);
lep_eta_MC->Fill(lep_eta_mc[index],weight);
Lepton.SetPtEtaPhiE(lep_pt_mc[index],lep_eta_mc[index],lep_phi_mc[index],lep_E_mc[index]);
MeT.SetPtEtaPhiE(MET_mc,0.0,MET_phi_mc,MET_mc);
mtw_MC->Fill(mTW,weight);

//--
double nbjet=0;
for(unsigned int j=0; j<jet_n_mc; j++)
{
jet_pt_MC ->Fill(jet_pt_mc[j],weight);
jet_eta_MC->Fill(jet_eta_mc[j],weight);
jet_JVF_MC->Fill(jet_jvf_mc[j],weight);
jet_MV1_MC->Fill(jet_MV1_mc[j],weight);
if(jet_MV1_mc[j]>=0.7892){nbjet++;}
}
//--
nbjets_MC->Fill(nbjet,weight); 
MET_MC->Fill(MET_mc,weight);
}










TCanvas *dif = new TCanvas("dif","dif",1600,1200);
dif->Divide(3,4);

dif->cd(1); 
lep_pt_wo->SetTitle("	");
lep_pt_wo->SetFillColor(kGreen);
lep_pt_wo->SetMinimum(0);
lep_pt_wo->GetYaxis()->SetTitle("Events");				
lep_pt_wo->GetXaxis()->SetTitle("pt"); //Change axis-titles
lep_pt_wo->Draw( "BAR2" );

dif->cd(2); 
lep_trackisolation_wo->SetTitle("	");
lep_trackisolation_wo->SetFillColor(kGreen);
lep_trackisolation_wo->SetMinimum(0);
lep_trackisolation_wo->GetYaxis()->SetTitle("Events");				
lep_trackisolation_wo->GetXaxis()->SetTitle("Track isolation"); //Change axis-titles
lep_trackisolation_wo->Draw( "BAR2");

dif->cd(3); 
lep_calorimeterisolation_wo->SetTitle("	");
lep_calorimeterisolation_wo->SetFillColor(kGreen);
lep_calorimeterisolation_wo->SetMinimum(0);
lep_calorimeterisolation_wo->GetYaxis()->SetTitle("Events");			
lep_calorimeterisolation_wo->GetXaxis()->SetTitle("Calorimeter isolation"); //Change axis-titles
lep_calorimeterisolation_wo->Draw("BAR2" );

dif->cd(4); 
lep_eta_wo->SetTitle("	");
lep_eta_wo->SetFillColor(kGreen);
lep_eta_wo->SetMinimum(0);
lep_eta_wo->GetYaxis()->SetTitle("Events");			
lep_eta_wo->GetXaxis()->SetTitle("Pseudorapidez"); //Change axis-titles
lep_eta_wo->Draw( "BAR2" );

dif->cd(5); 
mtw_wo->SetTitle("	");
mtw_wo->SetFillColor(kGreen);
mtw_wo->SetMinimum(0);
mtw_wo->GetYaxis()->SetTitle("Events");				
mtw_wo->GetXaxis()->SetTitle("mtw"); //Change axis-titles
mtw_wo->Draw( "BAR2" );

dif->cd(6); 
jet_pt_wo->SetTitle("	");
jet_pt_wo->SetFillColor(kGreen);
jet_pt_wo->SetMinimum(0);
jet_pt_wo->GetYaxis()->SetTitle("Events");				
jet_pt_wo->GetXaxis()->SetTitle("pt"); //Change axis-titles
jet_pt_wo->Draw( "BAR2" );

dif->cd(7); 
jet_eta_wo->SetTitle("	");
jet_eta_wo->SetFillColor(kGreen);
jet_eta_wo->SetMinimum(0);
jet_eta_wo->GetYaxis()->SetTitle("Events");			
jet_eta_wo->GetXaxis()->SetTitle("eta"); //Change axis-titles
jet_eta_wo->Draw("BAR2");

dif->cd(8); 
jet_JVF_wo->SetTitle("	");
jet_JVF_wo->SetFillColor(kGreen);
jet_JVF_wo->SetMinimum(0);
jet_JVF_wo->GetYaxis()->SetTitle("Events");				
jet_JVF_wo->GetXaxis()->SetTitle("JVF"); //Change axis-titles
jet_JVF_wo->Draw("BAR2");

dif->cd(9); 
jet_MV1_wo->SetTitle("	");
jet_MV1_wo->SetFillColor(kGreen);
jet_MV1_wo->SetMinimum(0);
jet_MV1_wo->GetYaxis()->SetTitle("Events");			
jet_MV1_wo->GetXaxis()->SetTitle("MV1"); //Change axis-titles
jet_MV1_wo->Draw("BAR2");

dif->cd(10); 
nbjets_wo->SetTitle("	");
nbjets_wo->SetFillColor(kGreen);
nbjets_wo->SetMinimum(0);
nbjets_wo->GetYaxis()->SetTitle("Events");				
nbjets_wo->GetXaxis()->SetTitle("nbjets"); //Change axis-titles
nbjets_wo->Draw("BAR2");

dif->cd(11); 
MET_wo->SetTitle("	");
MET_wo->SetFillColor(kGreen);
MET_wo->SetMinimum(0);
MET_wo->GetYaxis()->SetTitle("Events");				
MET_wo->GetXaxis()->SetTitle("MET"); //Change axis-titles
MET_wo->Draw("BAR2");

dif->Print("1.pdf");


TCanvas *dif1 = new TCanvas("dif1","dif1",1600,1200);
dif1->Divide(3,4);

dif1->cd(1); 
lep_pt_w->SetTitle("	");
lep_pt_w->SetFillColor(kGreen);
lep_pt_w->SetMinimum(0);
lep_pt_w->GetYaxis()->SetTitle("Events");				
lep_pt_w->GetXaxis()->SetTitle("pt"); //Change axis-titles
lep_pt_MC->SetMarkerStyle(20);
lep_pt_MC->SetMarkerSize(1.2);
lep_pt_w->Draw( "BAR2" );
lep_pt_MC->Draw( "PSAME" );


dif1->cd(2); 
lep_trackisolation_w->SetTitle("	");
lep_trackisolation_w->SetFillColor(kGreen);
lep_trackisolation_w->SetMinimum(0);
lep_trackisolation_w->GetYaxis()->SetTitle("Events");				
lep_trackisolation_w->GetXaxis()->SetTitle("Track isolation"); //Change axis-titles
lep_trackisolation_MC->SetMarkerStyle(20);
lep_trackisolation_MC->SetMarkerSize(1.2);
lep_trackisolation_w->Draw( "BAR2" );
lep_trackisolation_MC->Draw( "PSAME" );

dif1->cd(3); 
lep_calorimeterisolation_w->SetTitle("	");
lep_calorimeterisolation_w->SetFillColor(kGreen);
lep_calorimeterisolation_w->SetMinimum(0);
lep_calorimeterisolation_w->GetYaxis()->SetTitle("Events");			
lep_calorimeterisolation_w->GetXaxis()->SetTitle("Calorimeter isolation"); //Change axis-titles
lep_calorimeterisolation_MC->SetMarkerStyle(20);
lep_calorimeterisolation_MC->SetMarkerSize(1.2);
lep_calorimeterisolation_w->Draw( "BAR2" );
lep_calorimeterisolation_MC->Draw( "PSAME" );

dif1->cd(4); 
lep_eta_w->SetTitle("	");
lep_eta_w->SetFillColor(kGreen);
lep_eta_w->SetMinimum(0);
lep_eta_w->GetYaxis()->SetTitle("Events");			
lep_eta_w->GetXaxis()->SetTitle("Pseudorapidez"); //Change axis-titles
lep_eta_MC->SetMarkerStyle(20);
lep_eta_MC->SetMarkerSize(1.2);
lep_eta_w->Draw( "BAR2" );
lep_eta_MC->Draw( "PSAME" );

dif1->cd(5); 
mtw_w->SetTitle("	");
mtw_w->SetFillColor(kGreen);
mtw_w->SetMinimum(0);
mtw_w->GetYaxis()->SetTitle("Events");				
mtw_w->GetXaxis()->SetTitle("mtw"); //Change axis-titles
mtw_MC->SetMarkerStyle(20);
mtw_MC->SetMarkerSize(1.2);
mtw_w->Draw( "BAR2" );
mtw_MC->Draw( "PSAME" );

dif1->cd(6); 
jet_pt_w->SetTitle("	");
jet_pt_w->SetFillColor(kGreen);
jet_pt_w->SetMinimum(0);
jet_pt_w->GetYaxis()->SetTitle("Events");				
jet_pt_w->GetXaxis()->SetTitle("pt"); //Change axis-titles
jet_pt_MC->SetMarkerStyle(20);
jet_pt_MC->SetMarkerSize(1.2);
jet_pt_w->Draw( "BAR2" );
jet_pt_MC->Draw( "PSAME" );

dif1->cd(7); 
jet_eta_w->SetTitle("	");
jet_eta_w->SetFillColor(kGreen);
jet_eta_w->SetMinimum(0);
jet_eta_w->GetYaxis()->SetTitle("Events");			
jet_eta_w->GetXaxis()->SetTitle("eta"); //Change axis-titles
jet_eta_MC->SetMarkerStyle(20);
jet_eta_MC->SetMarkerSize(1.2);
jet_eta_w->Draw( "BAR2" );
jet_eta_MC->Draw( "PSAME" );

dif1->cd(8); 
jet_JVF_w->SetTitle("	");
jet_JVF_w->SetFillColor(kGreen);
jet_JVF_w->SetMinimum(0);
jet_JVF_w->GetYaxis()->SetTitle("Events");				
jet_JVF_w->GetXaxis()->SetTitle("JVF"); //Change axis-titles
jet_JVF_MC->SetMarkerStyle(20);
jet_JVF_MC->SetMarkerSize(1.2);
jet_JVF_w->Draw( "BAR2" );
jet_JVF_MC->Draw( "PSAME" );

dif1->cd(9); 
jet_MV1_w->SetTitle("	");
jet_MV1_w->SetFillColor(kGreen);
jet_MV1_w->SetMinimum(0);
jet_MV1_w->GetYaxis()->SetTitle("Events");			
jet_MV1_w->GetXaxis()->SetTitle("MV1"); //Change axis-titles
jet_MV1_MC->SetMarkerStyle(20);
jet_MV1_MC->SetMarkerSize(1.2);
jet_MV1_w->Draw( "BAR2" );
jet_MV1_MC->Draw( "PSAME" );

dif1->cd(10); 
nbjets_w->SetTitle("	");
nbjets_w->SetFillColor(kGreen);
nbjets_w->SetMinimum(0);
nbjets_w->GetYaxis()->SetTitle("Events");				
nbjets_w->GetXaxis()->SetTitle("nbjets"); //Change axis-titles
nbjets_MC->SetMarkerStyle(20);
nbjets_MC->SetMarkerSize(1.2);
nbjets_w->Draw( "BAR2" );
nbjets_MC->Draw( "PSAME" );

dif1->cd(11); 
MET_w->SetTitle("	");
MET_w->SetFillColor(kGreen);
MET_w->SetMinimum(0);
MET_w->GetYaxis()->SetTitle("Events");				
MET_w->GetXaxis()->SetTitle("MET"); //Change axis-titles
MET_MC->SetMarkerStyle(20);
MET_MC->SetMarkerSize(1.2);
MET_w->Draw( "BAR2" );
MET_MC->Draw( "PSAME" );

dif1->Print("2.pdf");



}
