#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>

using namespace RooFit;
using namespace RooStats;


void DATA8() 
{
TFile *file = TFile::Open("../Tracks_Clusters.root");
TTree *tree = (TTree*) file->Get("JetRecoTree");
//tree->Print();
//----------------------------------------------------------------------------------
float evtw = -1;

std::vector<float> *reco_R10_pt=0;
std::vector<float> *truth_R10_pt=0;
std::vector<float> *reco_R10_m=0;
std::vector<float> *truth_R10_m=0;

std::vector<float> *reco_R10_Trimmed_pt=0;
std::vector<float> *truth_R10_Trimmed_pt=0;
std::vector<float> *reco_R10_Trimmed_m=0;
std::vector<float> *truth_R10_Trimmed_m=0;


//----------------------------------------------------------------------------------
tree->SetBranchAddress("EventWeight", &evtw);

tree->SetBranchAddress("RecoJets_R10_pt", &reco_R10_pt);
tree->SetBranchAddress("TruthJets_R10_pt", &truth_R10_pt);
tree->SetBranchAddress("RecoJets_R10_m", &reco_R10_m);
tree->SetBranchAddress("TruthJets_R10_m", &truth_R10_m);
//
tree->SetBranchAddress("RecoJets_R10_Trimmed_pt", &reco_R10_Trimmed_pt);
tree->SetBranchAddress("TruthJets_R10_Trimmed_pt", &truth_R10_Trimmed_pt);
tree->SetBranchAddress("RecoJets_R10_Trimmed_m", &reco_R10_Trimmed_m);
tree->SetBranchAddress("TruthJets_R10_Trimmed_m", &truth_R10_Trimmed_m);


//----------------------------------------------------------------------------------
TH1F *hist_leadreco_10_pt_weights = new TH1F("Lead Reco-jet R10 (with weights)","Leading jet pT (with weights); pT(GeV);Events",50,10,500);

TH1F *hist_leadtruth_10_pt_weights = new TH1F("Lead truth-jet R10 (with weights)","Leading jet pT (with weights); pT(GeV);Events",50,10,500);

TH1F *hist_leadreco_10_m_weights = new TH1F("Lead Reco-jet R10 (with weights)","Leading jet m (with weights); m(GeV/c^2);Events",50,10,500);

TH1F *hist_leadtruth_10_m_weights = new TH1F("Lead truth-jet R10 (with weights)","Leading jet m (with weights); m(GeV/c^2);Events",50,10,500);
//-----------------
TH1F *hist_leadreco_10_trimmed_pt_weights = new TH1F("Lead Reco-jet R10 trimmed (with weights)","Leading jet pT (with weights); pT(GeV);Events",50,10,500);

TH1F *hist_leadtruth_10_trimmed_pt_weights = new TH1F("Lead truth-jet R10 trimmed (with weights)","Leading jet pT (with weights); pT(GeV);Events",50,10,500);

TH1F *hist_leadreco_10_trimmed_m_weights = new TH1F("Lead Reco-jet R10 trimmed (with weights)","Leading jet m (with weights); m(GeV/c^2);Events",50,10,500);

TH1F *hist_leadtruth_10_trimmed_m_weights = new TH1F("Lead truth-jet R10 trimmed (with weights)","Leading jet m (with weights); m(GeV/c^2);Events",50,10,500);


//----------------------------------------------------------------------------------
int nentries=tree->GetEntries();
std::cout<<nentries<<std::endl;
for (Long64_t ievt=0; ievt<nentries; ievt++)
{
if((ievt%1000)==0){std::cout<<ievt<<std::endl;}
tree->GetEntry(ievt);

	if(reco_R10_pt->size()>0)
	{hist_leadreco_10_pt_weights->Fill(reco_R10_pt->at(0)/1000.,evtw); hist_leadreco_10_m_weights->Fill(reco_R10_m->at(0)/1000.,evtw);}

    	if(truth_R10_pt->size()>0)
	{hist_leadtruth_10_pt_weights->Fill(truth_R10_pt->at(0)/1000.,evtw); hist_leadtruth_10_m_weights->Fill(truth_R10_m->at(0)/1000.,evtw);}

	if(reco_R10_Trimmed_pt->size()>0)
	{hist_leadreco_10_trimmed_pt_weights->Fill(reco_R10_Trimmed_pt->at(0)/1000.,evtw); hist_leadreco_10_trimmed_m_weights->Fill(reco_R10_Trimmed_m->at(0)/1000.,evtw);}

    	if(truth_R10_Trimmed_pt->size()>0)
	{hist_leadtruth_10_trimmed_pt_weights->Fill(truth_R10_Trimmed_pt->at(0)/1000.,evtw); hist_leadtruth_10_trimmed_m_weights->Fill(truth_R10_Trimmed_m->at(0)/1000.,evtw);}

}


//----------------------------------------------------------------------------------
auto legend1 = new TLegend(0.60,0.60,0.90,0.80);
//legend1->SetBorderSize(0);
legend1->SetTextSize(0.02);
legend1->AddEntry(hist_leadreco_10_pt_weights,"Leading reco R10 jet PT");
legend1->AddEntry(hist_leadreco_10_trimmed_pt_weights,"Leading reco R10 trimmed jet PT");

auto legend2 = new TLegend(0.60,0.60,0.90,0.80);
//legend2->SetBorderSize(0);
legend2->SetTextSize(0.02);
legend2->AddEntry(hist_leadtruth_10_pt_weights,"Leading truth R10 jet PT");
legend2->AddEntry(hist_leadtruth_10_trimmed_pt_weights,"Leading truth R10 trimmed jet PT");


auto legend3 = new TLegend(0.60,0.60,0.90,0.80);
//legend3->SetBorderSize(0);
legend3->SetTextSize(0.02);
legend3->AddEntry(hist_leadreco_10_m_weights,"Leading reco R10 jet m");
legend3->AddEntry(hist_leadreco_10_trimmed_m_weights,"Leading reco R10 trimmed jet m");

auto legend4 = new TLegend(0.60,0.60,0.90,0.80);
//legend4->SetBorderSize(0);
legend4->SetTextSize(0.02);
legend4->AddEntry(hist_leadtruth_10_m_weights,"Leading truth R10 jet m");
legend4->AddEntry(hist_leadtruth_10_trimmed_m_weights,"Leading truth R10 trimmed jet m");




//----------------------------------------------------------------------------------

TCanvas *canvas12 = new TCanvas("file12","file12",3200,900);
hist_leadreco_10_pt_weights->SetStats(0);
hist_leadreco_10_pt_weights->SetMarkerStyle(20);
hist_leadreco_10_pt_weights->SetMarkerColor(kRed);
hist_leadreco_10_pt_weights->SetMinimum(1);
hist_leadreco_10_pt_weights->Draw("E");
hist_leadreco_10_trimmed_pt_weights->SetMarkerStyle(21);
hist_leadreco_10_trimmed_pt_weights->Draw("Esame");
canvas12->SetLogy();
legend1->Draw("same");
canvas12->Print("problema81.png");
canvas12->Clear();
//------

TCanvas *canvas13 = new TCanvas("file13","file13",3200,900);
hist_leadtruth_10_pt_weights->SetStats(0);
hist_leadtruth_10_pt_weights->SetMarkerStyle(20);
hist_leadtruth_10_pt_weights->SetMarkerColor(kRed);
hist_leadtruth_10_pt_weights->SetMinimum(1);
hist_leadtruth_10_pt_weights->Draw("E");
hist_leadtruth_10_trimmed_pt_weights->SetMarkerStyle(21);
hist_leadtruth_10_trimmed_pt_weights->Draw("Esame");
canvas13->SetLogy();
legend2->Draw("same");
canvas13->Print("problema82.png");
canvas13->Clear();
//------

TCanvas *canvas14 = new TCanvas("file14","file14",3200,900);
hist_leadreco_10_m_weights->SetStats(0);
hist_leadreco_10_m_weights->SetMarkerStyle(20);
hist_leadreco_10_m_weights->SetMarkerColor(kRed);
hist_leadreco_10_m_weights->SetMinimum(1);
hist_leadreco_10_m_weights->Draw("E");
hist_leadreco_10_trimmed_m_weights->SetMarkerStyle(21);
hist_leadreco_10_trimmed_m_weights->Draw("Esame");
canvas14->SetLogy();
legend3->Draw("same");
canvas14->Print("problema83.png");
canvas14->Clear();
//------

TCanvas *canvas15 = new TCanvas("file15","file15",3200,900);
hist_leadtruth_10_m_weights->SetStats(0);
hist_leadtruth_10_m_weights->SetMarkerStyle(20);
hist_leadtruth_10_m_weights->SetMarkerColor(kRed);
hist_leadtruth_10_m_weights->SetMinimum(1);
hist_leadtruth_10_m_weights->Draw("E");
hist_leadtruth_10_trimmed_m_weights->SetMarkerStyle(21);
hist_leadtruth_10_trimmed_m_weights->Draw("Esame");
canvas15->SetLogy();
legend4->Draw("same");
canvas15->Print("problema84.png");
canvas15->Clear();
//------


}
