#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>

using namespace RooFit;
using namespace RooStats;


void DATA3() 
{
TFile *file = TFile::Open("../Tracks_Clusters.root");
TTree *tree = (TTree*) file->Get("JetRecoTree");
//tree->Print();
//----------------------------------------------------------------------------------
UInt_t npv = -1;
float evtw = -1;
std::vector<float> *reco_R4_pt=0;
std::vector<float> *truth_R4_pt=0;

std::vector<float> *reco_R10_pt=0;
std::vector<float> *truth_R10_pt=0;

std::vector<float> *reco_R10_Trimmed_pt=0;
std::vector<float> *truth_R10_Trimmed_pt=0;
//----------------------------------------------------------------------------------
tree->SetBranchAddress("NPV", &npv);
tree->SetBranchAddress("EventWeight", &evtw);
//
tree->SetBranchAddress("RecoJets_R4_pt", &reco_R4_pt);
tree->SetBranchAddress("TruthJets_R4_pt", &truth_R4_pt);
//
tree->SetBranchAddress("RecoJets_R10_pt", &reco_R10_pt);
tree->SetBranchAddress("TruthJets_R10_pt", &truth_R10_pt);
//
tree->SetBranchAddress("RecoJets_R10_Trimmed_pt", &reco_R10_Trimmed_pt);
tree->SetBranchAddress("TruthJets_R10_Trimmed_pt", &truth_R10_Trimmed_pt);

//----------------------------------------------------------------------------------
TH2F *hist_recojetpt_npv = new TH2F("Reco-jet R4 pT vs. NPV",";NPV; jet pT",100,0,300, 20, 0, 100);
TH2F *hist_truthjetpt_npv = new TH2F("Truth-jet R4 pT vs. NPV",";NPV; jet pT",100,0,300, 20, 0, 100);
//--------------------
TProfile *prof_recojetpt_npv = new TProfile("Profile Reco-jet R4 pT vs. NPV",";NPV; jet pT",100,0,300, 0, 80);
TProfile *prof_truthjetpt_npv = new TProfile("Truth-jet R4 pT vs. NPV",";NPV; jet pT",100,0,300, 0, 80);
//------------------------------------------------------
TH2F *hist_recojetpt_r10_npv = new TH2F("Reco-jet R10 pT vs. NPV",";NPV; jet pT",100,50,300, 20, 0, 100);
TH2F *hist_truthjetpt_r10_npv = new TH2F("Truth-jet R10 pT vs. NPV",";NPV; jet pT",100,50,300, 20, 0, 100);
//--------------------
TProfile *prof_recojetpt_r10_npv = new TProfile("Profile Reco-jet R10 pT vs. NPV",";NPV; jet pT",100,50,300, 0, 80);
TProfile *prof_truthjetpt_r10_npv = new TProfile("Truth-jet R10 pT vs. NPV",";NPV; jet pT",100,50,300, 0, 80);
//------------------------------------------------------
TH2F *hist_recojetpt_r10_trimmed_npv = new TH2F("Reco-jet R10 Trimmed pT vs. NPV",";NPV; jet pT",100,0,300, 20, 0, 100);
TH2F *hist_truthjetpt_r10_trimmed_npv = new TH2F("Truth-jet R10 Trimmed pT vs. NPV",";NPV; jet pT",100,0,300, 20, 0, 100);
//--------------------
TProfile *prof_recojetpt_r10_trimmed_npv = new TProfile("Profile Reco-jet R10 Trimmed pT vs. NPV",";NPV; jet pT",100,0,300, 0, 80);
TProfile *prof_truthjetpt_r10_trimmed_npv = new TProfile("Truth-jet R10 Trimmed pT vs. NPV",";NPV; jet pT",100,0,300, 0, 80);

//----------------------------------------------------------------------------------
int nentries=tree->GetEntries();
std::cout<<nentries<<std::endl;
for (Long64_t ievt=0; ievt<nentries; ievt++)
{
if((ievt%1000)==0){std::cout<<ievt<<std::endl;}
tree->GetEntry(ievt);
	if(reco_R4_pt->size()>0)
	{	if(reco_R4_pt->at(0)>20000.)
		{
	 		for(int j=0; j<reco_R4_pt->size(); j++)
	 		{
			
			hist_recojetpt_npv->Fill(reco_R4_pt->at(j)/1000.,npv,evtw);
			prof_recojetpt_npv->Fill(reco_R4_pt->at(j)/1000.,npv,evtw);

			}
		}
    	}

    	if(truth_R4_pt->size()>0)
	{	if(truth_R4_pt->at(0)>20000.)
		{
			for(int j=0; j<truth_R4_pt->size(); j++)
			{
			hist_truthjetpt_npv->Fill(truth_R4_pt->at(j)/1000.,npv,evtw);
			prof_truthjetpt_npv->Fill(truth_R4_pt->at(j)/1000.,npv,evtw);
			}
		}
    	}


	if(reco_R10_pt->size()>0)
	{	if(reco_R10_pt->at(0)>0000.)
		{
	 		for(int j=0; j<reco_R10_pt->size(); j++)
	 		{
			
			hist_recojetpt_r10_npv->Fill(reco_R10_pt->at(j)/1000.,npv,evtw);
			prof_recojetpt_r10_npv->Fill(reco_R10_pt->at(j)/1000.,npv,evtw);

			}
		}
    	}

    	if(truth_R10_pt->size()>0)
	{	if(truth_R10_pt->at(0)>20000.)
		{
			for(int j=0; j<truth_R10_pt->size(); j++)
			{
			hist_truthjetpt_r10_npv->Fill(truth_R10_pt->at(j)/1000.,npv,evtw);
			prof_truthjetpt_r10_npv->Fill(truth_R10_pt->at(j)/1000.,npv,evtw);
			}
		}
    	}

	if(reco_R10_Trimmed_pt->size()>0)
	{	if(reco_R10_Trimmed_pt->at(0)>20000.)
		{
	 		for(int j=0; j<reco_R10_pt->size(); j++)
	 		{
			
			hist_recojetpt_r10_trimmed_npv->Fill(reco_R10_Trimmed_pt->at(j)/1000.,npv,evtw);
			prof_recojetpt_r10_trimmed_npv->Fill(reco_R10_Trimmed_pt->at(j)/1000.,npv,evtw);

			}
		}
    	}

    	if(truth_R10_Trimmed_pt->size()>0)
	{	if(truth_R10_Trimmed_pt->at(0)>20000.)
		{
			for(int j=0; j<truth_R10_pt->size(); j++)
			{
			hist_truthjetpt_r10_trimmed_npv->Fill(truth_R10_Trimmed_pt->at(j)/1000.,npv,evtw);
			prof_truthjetpt_r10_trimmed_npv->Fill(truth_R10_Trimmed_pt->at(j)/1000.,npv,evtw);
			}
		}
    	}
}

//----------------------------------------------------------------------------------
TCanvas *canvas31 = new TCanvas("file31","file31",3200,900);
canvas31->Divide(2,1);
canvas31->cd(1);
hist_truthjetpt_npv->Draw("colz");
canvas31->cd(2);
hist_recojetpt_npv->Draw("colz");
canvas31->Print("problema31.png");
//------
TCanvas *canvas32 = new TCanvas("file32","file32",3200,900);
canvas32->Divide(2,1);
canvas32->cd(1);
hist_truthjetpt_r10_npv->Draw("colz");
canvas32->cd(2);
hist_recojetpt_r10_npv->Draw("colz");
canvas32->Print("problema32.png");
//------
TCanvas *canvas33 = new TCanvas("file33","file33",3200,900);
canvas33->Divide(2,1);
canvas33->cd(1);
hist_truthjetpt_r10_trimmed_npv->Draw("colz");
canvas33->cd(2);
hist_recojetpt_r10_trimmed_npv->Draw("colz");
canvas33->Print("problema33.png");
//------
TCanvas *canvas34 = new TCanvas("file34","file34",3200,900);
canvas34->Divide(2,1);
canvas34->cd(1);
prof_recojetpt_npv->Draw("");
prof_recojetpt_npv->SetMaximum(50);
prof_recojetpt_npv->SetMinimum(15);
canvas34->cd(2);
prof_truthjetpt_npv->Draw("");
prof_truthjetpt_npv->SetMaximum(50);
prof_truthjetpt_npv->SetMinimum(15);
canvas34->Print("problema34.png");
//------
TCanvas *canvas35 = new TCanvas("file35","file35",3200,900);
canvas35->Divide(2,1);
canvas35->cd(1);
prof_recojetpt_r10_npv->Draw("");
prof_recojetpt_r10_npv->SetMaximum(50);
prof_recojetpt_r10_npv->SetMinimum(15);
canvas35->cd(2);
prof_truthjetpt_r10_npv->Draw("");
prof_truthjetpt_r10_npv->SetMaximum(50);
prof_truthjetpt_r10_npv->SetMinimum(15);
canvas35->Print("problema35.png");
//------
TCanvas *canvas36 = new TCanvas("file36","file36",3200,900);
canvas36->Divide(2,1);
canvas36->cd(1);
prof_recojetpt_r10_trimmed_npv->Draw("");
prof_recojetpt_r10_trimmed_npv->SetMaximum(50);
prof_recojetpt_r10_trimmed_npv->SetMinimum(15);
canvas36->cd(2);
prof_truthjetpt_r10_trimmed_npv->Draw("");
prof_truthjetpt_r10_trimmed_npv->SetMaximum(50);
prof_truthjetpt_r10_trimmed_npv->SetMinimum(15);
canvas36->Print("problema36.png");
//------
}
