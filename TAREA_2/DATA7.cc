#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>

using namespace RooFit;
using namespace RooStats;


void DATA7() 
{
TFile *file = TFile::Open("../Tracks_Clusters.root");
TTree *tree = (TTree*) file->Get("JetRecoTree");
//tree->Print();
//----------------------------------------------------------------------------------
UInt_t npv = -1;
float evtw = -1;
std::vector<float> *reco_R4_pt=0;
std::vector<float> *reco_R4_jvf=0;
std::vector<float> *reco_R4_eta=0;
std::vector<float> *reco_R4_phi=0;
std::vector<float> *reco_R4_m=0;

std::vector<float> *truth_R4_pt=0;
std::vector<float> *truth_R4_eta=0;
std::vector<float> *truth_R4_phi=0;
std::vector<float> *truth_R4_m=0;

std::vector<float> *track_R4_pt=0;
std::vector<float> *track_R4_eta=0;
std::vector<float> *track_R4_phi=0;
std::vector<float> *track_R4_m=0;
//----------------------------------------------------------------------------------
tree->SetBranchAddress("NPV", &npv);
tree->SetBranchAddress("EventWeight", &evtw);

tree->SetBranchAddress("RecoJets_R4_pt", &reco_R4_pt);
tree->SetBranchAddress("RecoJets_R4_jvf", &reco_R4_jvf);
tree->SetBranchAddress("RecoJets_R4_eta", &reco_R4_eta);
tree->SetBranchAddress("RecoJets_R4_phi", &reco_R4_phi);
tree->SetBranchAddress("RecoJets_R4_m", &reco_R4_m);

tree->SetBranchAddress("TruthJets_R4_pt", &truth_R4_pt);
tree->SetBranchAddress("TruthJets_R4_eta", &truth_R4_eta);
tree->SetBranchAddress("TruthJets_R4_phi", &truth_R4_phi);
tree->SetBranchAddress("TruthJets_R4_m", &truth_R4_m);

tree->SetBranchAddress("TrackJets_R4_pt", &track_R4_pt);
tree->SetBranchAddress("TrackJets_R4_eta", &track_R4_eta);
tree->SetBranchAddress("TrackJets_R4_phi", &track_R4_phi);
tree->SetBranchAddress("TrackJets_R4_m", &track_R4_m);
//----------------------------------------------------------------------------------
TH1F *hist_frac_reco_truth_20 = new TH1F("Frac reco truth 20","Frac; #Frac; Events",80,0,4);
TH1F *hist_frac_track_truth_20 = new TH1F("Frac track truth 20","Frac; #Frac; Events",80,0,4);
TH1F *hist_frac_reco_truth_100 = new TH1F("Frac reco truth 100","Frac; #Frac; Events",80,0,4);
TH1F *hist_frac_track_truth_100 = new TH1F("Frac track truth 100","Frac; #Frac; Events",80,0,4);
TH1F *hist_frac_reco_truth_500 = new TH1F("Frac reco truth 500","Frac; #Frac; Events",80,0,4);
TH1F *hist_frac_track_truth_500 = new TH1F("Frac track truth 500","Frac; #Frac; Events",80,0,4);

//----------------------------------------------------------------------------------
int nentries=tree->GetEntries();
std::cout<<nentries<<std::endl;
double val;
for (Long64_t ievt=0; ievt<nentries; ievt++)
{
if((ievt%1000)==0){std::cout<<ievt<<std::endl;}
tree->GetEntry(ievt);
    	if(truth_R4_pt->size()>0 && truth_R4_pt->at(0)>20000.)
	{TLorentzVector truthJet;
	 truthJet.SetPtEtaPhiM(truth_R4_pt->at(0),truth_R4_eta->at(0),truth_R4_phi->at(0),truth_R4_m->at(0));        
		if(reco_R4_pt->size()>0)
		{
		TLorentzVector recoJet;
		recoJet.SetPtEtaPhiM(reco_R4_pt->at(0),reco_R4_eta->at(0),reco_R4_phi->at(0),reco_R4_m->at(0)); 
			if(truthJet.DeltaR(recoJet)<0.3)
			{
			val=reco_R4_pt->at(0)/truth_R4_pt->at(0);
			hist_frac_reco_truth_20->Fill(val,evtw);
				if(truth_R4_pt->at(0)>100000.)
				{
				hist_frac_reco_truth_100->Fill(val,evtw);
					if(truth_R4_pt->at(0)>500000.)
					{hist_frac_reco_truth_500->Fill(val,evtw);}
				}
			}
       		}
		if(track_R4_pt->size()>0)
		{
		TLorentzVector trackJet;
		trackJet.SetPtEtaPhiM(track_R4_pt->at(0),track_R4_eta->at(0),track_R4_phi->at(0),track_R4_m->at(0)); 
			if(truthJet.DeltaR(trackJet)<0.3)
			{
			val=track_R4_pt->at(0)/truth_R4_pt->at(0);
			hist_frac_track_truth_20->Fill(val,evtw);
				if(truth_R4_pt->at(0)>100000.)
				{
				hist_frac_track_truth_100->Fill(val,evtw);
					if(truth_R4_pt->at(0)>500000.)
					{hist_frac_track_truth_500->Fill(val,evtw);}
				}
			}
       		}
    	}
}

//----------------------------------------------------------------------------------
auto legend1 = new TLegend(0.50,0.30,0.90,0.60);
//legend1->SetBorderSize(0);
legend1->SetTextSize(0.03);
legend1->AddEntry(hist_frac_reco_truth_20, "#frac{Pt_{reco}}{Pt_{truth}}, Pt_{truth}>20GeV");
legend1->AddEntry(hist_frac_reco_truth_100,"#frac{Pt_{reco}}{Pt_{truth}}, Pt_{truth}>100GeV");
legend1->AddEntry(hist_frac_reco_truth_500,"#frac{Pt_{reco}}{Pt_{truth}}, Pt_{truth}>500GeV");


auto legend2 = new TLegend(0.50,0.30,0.90,0.60);
//legend2->SetBorderSize(0);
legend2->SetTextSize(0.03);
legend2->AddEntry(hist_frac_track_truth_20, "#frac{Pt_{track}}{Pt_{truth}}, Pt_{truth}>20GeV");
legend2->AddEntry(hist_frac_track_truth_100,"#frac{Pt_{track}}{Pt_{truth}}, Pt_{truth}>100GeV");
legend2->AddEntry(hist_frac_track_truth_500,"#frac{Pt_{track}}{Pt_{truth}}, Pt_{truth}>500GeV");


//----------------------------------------------------------------------------------
TCanvas *canvas71 = new TCanvas("file71","file71",1600,900);
hist_frac_reco_truth_500->Scale(1/hist_frac_reco_truth_500->Integral());
hist_frac_reco_truth_500->SetStats(0);
hist_frac_reco_truth_500->SetMinimum(0);
hist_frac_reco_truth_500->SetMarkerStyle(20);
hist_frac_reco_truth_500->SetMarkerColor(kGreen);
hist_frac_reco_truth_500->DrawNormalized("");
hist_frac_reco_truth_20->Scale(1/hist_frac_reco_truth_20->Integral());
hist_frac_reco_truth_20->SetStats(0);
hist_frac_reco_truth_20->SetMarkerStyle(20);
hist_frac_reco_truth_20->SetMarkerColor(kRed);
hist_frac_reco_truth_20->DrawNormalized("same");
hist_frac_reco_truth_100->Scale(1/hist_frac_reco_truth_100->Integral());
hist_frac_reco_truth_100->SetStats(0);
hist_frac_reco_truth_100->SetMarkerStyle(20);
hist_frac_reco_truth_100->DrawNormalized("same");
legend1->Draw("same");
canvas71->Print("problema71.png");
canvas71->Clear();

TCanvas *canvas72 = new TCanvas("file72","file72",1600,900);
hist_frac_track_truth_500->Scale(1/hist_frac_track_truth_500->Integral());
hist_frac_track_truth_500->SetStats(0);
hist_frac_track_truth_500->SetMinimum(0);
hist_frac_track_truth_500->SetMarkerStyle(20);
hist_frac_track_truth_500->SetMarkerColor(kGreen);
hist_frac_track_truth_500->DrawNormalized("");
hist_frac_track_truth_20->Scale(1/hist_frac_track_truth_20->Integral());
hist_frac_track_truth_20->SetStats(0);
hist_frac_track_truth_20->SetMarkerStyle(20);
hist_frac_track_truth_20->SetMarkerColor(kRed);
hist_frac_track_truth_20->DrawNormalized("same");
hist_frac_track_truth_100->Scale(1/hist_frac_track_truth_100->Integral());
hist_frac_track_truth_100->SetStats(0);
hist_frac_track_truth_100->SetMarkerStyle(20);
hist_frac_track_truth_100->DrawNormalized("same");
legend2->Draw("same");
canvas72->Print("problema72.png");
canvas72->Clear();

//------
}
