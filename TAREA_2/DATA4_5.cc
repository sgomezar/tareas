#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>

using namespace RooFit;
using namespace RooStats;


void DATA4_5() 
{
TFile *file = TFile::Open("../Tracks_Clusters.root");
TTree *tree = (TTree*) file->Get("JetRecoTree");
//tree->Print();
//----------------------------------------------------------------------------------
UInt_t npv = -1;
float evtw = -1;
std::vector<float> *reco_R4_pt=0;
std::vector<float> *reco_R4_jvf=0;


std::vector<float> *truth_R4_pt=0;

std::vector<float> *track_R4_pt=0;
//----------------------------------------------------------------------------------
tree->SetBranchAddress("NPV", &npv);

tree->SetBranchAddress("EventWeight", &evtw);

tree->SetBranchAddress("RecoJets_R4_pt", &reco_R4_pt);
tree->SetBranchAddress("RecoJets_R4_jvf", &reco_R4_jvf);

tree->SetBranchAddress("TruthJets_R4_pt", &truth_R4_pt);

tree->SetBranchAddress("TrackJets_R4_pt", &track_R4_pt);
//----------------------------------------------------------------------------------
TProfile *prof_leadrecojetpt_npv_wocut = new TProfile("Profile Leading Reco-jet pT vs. NPV (without cut)",";NPV; jet pT",50,0,50, 0, 200);
TProfile *prof_leadrecojetpt_npv_wcut  = new TProfile("Profile Leading Reco-jet pT vs. NPV (with cut)",";NPV; jet pT",50,0,50, 0, 200);
TProfile *prof_leadtruthjetpt_npv      = new TProfile("Profile Leading Truth-jet pT vs. NPV",";NPV; jet pT",50,0,50, 0, 200);
TProfile *prof_leadtrackjetpt_npv      = new TProfile("Profile Leading Track-jet pT vs. NPV",";NPV; jet pT",50,0,50, 0, 200);
//----------------------------------------------------------------------------------
int nentries=tree->GetEntries();
std::cout<<nentries<<std::endl;
for (Long64_t ievt=0; ievt<nentries; ievt++)
{
if((ievt%1000)==0){std::cout<<ievt<<std::endl;}
tree->GetEntry(ievt);
	if(reco_R4_pt->size()>0)
	{
	 prof_leadrecojetpt_npv_wocut->Fill(reco_R4_pt->at(0)/1000.,npv,evtw);
	 if(std::abs(reco_R4_jvf->at(0))>0.5){prof_leadrecojetpt_npv_wcut->Fill(reco_R4_pt->at(0)/1000.,npv,evtw);}
    	}

    	if(truth_R4_pt->size()>0)
	{prof_leadtruthjetpt_npv->Fill(truth_R4_pt->at(0)/1000.,npv,evtw);}

    	if(track_R4_pt->size()>0)
	{prof_leadtrackjetpt_npv->Fill(track_R4_pt->at(0)/1000.,npv,evtw);}
}

//----------------------------------------------------------------------------------
auto legend1 = new TLegend(0.55,0.10,0.90,0.30);
//legend1->SetBorderSize(0);
legend1->SetTextSize(0.02);
legend1->AddEntry(prof_leadrecojetpt_npv_wocut,"Profile Leading reco jet PT vs NPV (without cut)");
legend1->AddEntry(prof_leadrecojetpt_npv_wcut,"Profile Leading reco jet PT vs NPV (with cut)");
legend1->AddEntry(prof_leadtruthjetpt_npv,"Profile Leading truth jet PT vs NPV");
legend1->AddEntry(prof_leadtrackjetpt_npv,"Profile Leading track jet PT vs NPV");

//----------------------------------------------------------------------------------
TCanvas *canvas4 = new TCanvas("file4","file4",1600,900);
canvas4->Divide(2,2);
canvas4->cd(1);
prof_leadtrackjetpt_npv->SetStats(0);
prof_leadtrackjetpt_npv->SetMarkerStyle(20);
prof_leadtrackjetpt_npv->SetMarkerColor(kGreen);
prof_leadtrackjetpt_npv->Draw("");
prof_leadrecojetpt_npv_wocut->SetStats(0);
prof_leadrecojetpt_npv_wocut->SetMarkerStyle(20);
prof_leadrecojetpt_npv_wocut->Draw("same");
prof_leadrecojetpt_npv_wcut->SetStats(0);
prof_leadrecojetpt_npv_wcut->SetMarkerStyle(20);
prof_leadrecojetpt_npv_wcut->SetMarkerColor(kRed);
prof_leadrecojetpt_npv_wcut->Draw("same");
prof_leadtruthjetpt_npv->SetStats(0);
prof_leadtruthjetpt_npv->SetMarkerStyle(20);
prof_leadtruthjetpt_npv->SetMarkerColor(kBlue);
prof_leadtruthjetpt_npv->Draw("same");
legend1->Draw("same");

canvas4->cd(2);
//prof_leadrecojetpt_npv_wocut->SetStats(1);
prof_leadrecojetpt_npv_wocut->SetMarkerStyle(20);
prof_leadrecojetpt_npv_wocut->Draw("");
prof_leadrecojetpt_npv_wcut->SetMarkerStyle(20);
prof_leadrecojetpt_npv_wcut->SetMarkerColor(kRed);
prof_leadrecojetpt_npv_wcut->Draw("same");

canvas4->cd(3);
prof_leadtruthjetpt_npv->SetMarkerStyle(20);
prof_leadtruthjetpt_npv->SetMarkerColor(kBlue);
prof_leadtruthjetpt_npv->Draw("");

canvas4->cd(4);

prof_leadtrackjetpt_npv->SetMarkerStyle(20);
prof_leadtrackjetpt_npv->SetMarkerColor(kGreen);
prof_leadtrackjetpt_npv->Draw("");

canvas4->Print("problema4.png");
canvas4->Clear();
//------
}
