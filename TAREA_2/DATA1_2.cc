#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>

using namespace RooFit;
using namespace RooStats;


void DATA1_2() 
{
TFile *file = TFile::Open("../Tracks_Clusters.root");
TTree *tree = (TTree*) file->Get("JetRecoTree");
//tree->Print();
//----------------------------------------------------------------------------------
float evtw = -1;

std::vector<float> *reco_R4_pt=0;
std::vector<float> *truth_R4_pt=0;
std::vector<float> *track_R4_pt=0;
//----------------------------------------------------------------------------------
tree->SetBranchAddress("EventWeight", &evtw);
tree->SetBranchAddress("RecoJets_R4_pt", &reco_R4_pt);
tree->SetBranchAddress("TruthJets_R4_pt", &truth_R4_pt);
tree->SetBranchAddress("TrackJets_R4_pt", &track_R4_pt);
//----------------------------------------------------------------------------------
TH1F *hist_leadreco_pt_noweights = new TH1F("Lead Reco-jet R4 (no weights)","Leading jet pT (no weights); pT(GeV);Events",50,10,500);
TH1F *hist_leadreco_pt_weights = new TH1F("Lead Reco-jet R4 (with weights)","Leading jet pT (with weights); pT(GeV);Events",50,10,500);
//--------------------
TH1F *hist_leadtruth_pt_noweights = new TH1F("Lead truth-jet R4 (no weights)","Leading jet pT (no weights); pT(GeV);Events",50,10,500);
TH1F *hist_leadtruth_pt_weights = new TH1F("Lead truth-jet R4 (with weights)","Leading jet pT (with weights); pT(GeV);Events",50,10,500);
//--------------------
TH1F *hist_leadtrack_pt_noweights = new TH1F("Lead track-jet (no weights)","Leading jet pT (no weights); pT(GeV);Events",50,10,500);
TH1F *hist_leadtrack_pt_weights = new TH1F("Lead track-jet (with weights)","Leading jet pT (with weights); pT(GeV);Events",50,10,500);
//--------------------

//----------------------------------------------------------------------------------
int nentries=tree->GetEntries();
std::cout<<nentries<<std::endl;
for (Long64_t ievt=0; ievt<nentries; ievt++)
{
if((ievt%1000)==0){std::cout<<ievt<<std::endl;}
tree->GetEntry(ievt);
	if(reco_R4_pt->size()>0)
	{hist_leadreco_pt_weights->Fill(reco_R4_pt->at(0)/1000.,evtw);
	 hist_leadreco_pt_noweights->Fill(reco_R4_pt->at(0)/1000.);
    	}
    	if(truth_R4_pt->size()>0)
	{hist_leadtruth_pt_weights->Fill(truth_R4_pt->at(0)/1000.,evtw);
	 hist_leadtruth_pt_noweights->Fill(truth_R4_pt->at(0)/1000.); 
    	}
    	if(track_R4_pt->size()>0)
	{hist_leadtrack_pt_weights->Fill(track_R4_pt->at(0)/1000.,evtw);
	 hist_leadtrack_pt_noweights->Fill(track_R4_pt->at(0)/1000.); 
    	}
}


//----------------------------------------------------------------------------------
auto legend1 = new TLegend(0.50,0.40,0.85,0.60);
//legend1->SetBorderSize(0);
legend1->SetTextSize(0.02);
legend1->AddEntry(hist_leadreco_pt_weights,"Leading reco jet PT (weighted)");
legend1->AddEntry(hist_leadtruth_pt_weights,"Leading truth jet PT (weighted)");

auto legend2 = new TLegend(0.50,0.22,0.85,0.42);
//legend2->SetBorderSize(0);
legend2->SetTextSize(0.02);
legend2->AddEntry(hist_leadreco_pt_weights,"Leading reco jet PT (no weighted)");
legend2->AddEntry(hist_leadtruth_pt_weights,"Leading truth jet PT (no weighted)");

//----------------------------------------------------------------------------------
TCanvas *canvas11 = new TCanvas("file11","file11",3200,900);
canvas11->Divide(2,1);
canvas11->cd(1);
hist_leadreco_pt_weights->SetStats(0);
hist_leadreco_pt_weights->SetMarkerStyle(20);
hist_leadreco_pt_weights->SetMarkerColor(kRed);
hist_leadreco_pt_weights->SetMinimum(1);
hist_leadreco_pt_weights->Draw("E");
hist_leadtruth_pt_weights->SetMarkerStyle(21);
hist_leadtruth_pt_weights->Draw("Esame");
legend1->Draw("same");
canvas11->cd(1)->SetLogy();

canvas11->cd(2);
hist_leadreco_pt_noweights->SetStats(0);
hist_leadreco_pt_noweights->SetMarkerStyle(20);
hist_leadreco_pt_noweights->SetMarkerColor(kRed);
hist_leadreco_pt_noweights->SetMinimum(1);
hist_leadreco_pt_noweights->Draw("E");
hist_leadtruth_pt_noweights->SetMarkerStyle(21);
hist_leadtruth_pt_noweights->Draw("Esame");
legend2->Draw("same");
canvas11->cd(2)->SetLogy();

canvas11->Print("problema11.png");
canvas11->Clear();
//------
TCanvas *canvas14 = new TCanvas("file14","file14",3200,900);
canvas14->Divide(2,1);
canvas14->cd(1);
hist_leadtrack_pt_weights->SetMarkerStyle(20);
hist_leadtrack_pt_weights->SetMarkerColor(kRed);
hist_leadtrack_pt_weights->SetMinimum(1);
hist_leadtrack_pt_weights->Draw("E");
canvas14->cd(1)->SetLogy();

canvas14->cd(2);
hist_leadtrack_pt_noweights->SetMarkerStyle(20);
hist_leadtrack_pt_noweights->SetMarkerColor(kRed);
hist_leadtrack_pt_noweights->SetMinimum(1);
hist_leadtrack_pt_noweights->Draw("E");
canvas14->cd(2)->SetLogy();

canvas14->Print("problema12.png");
canvas14->Clear();
//------


}
