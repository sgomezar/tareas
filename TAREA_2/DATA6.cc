#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>

using namespace RooFit;
using namespace RooStats;


void DATA6() 
{
TFile *file = TFile::Open("../Tracks_Clusters.root");
TTree *tree = (TTree*) file->Get("JetRecoTree");
//tree->Print();
//----------------------------------------------------------------------------------
UInt_t npv = -1;
float mu = -1;
float evtw = -1;
std::vector<float> *reco_R4_pt=0;
std::vector<float> *reco_R4_jvf=0;
std::vector<float> *reco_R4_eta=0;
std::vector<float> *reco_R4_phi=0;
std::vector<float> *reco_R4_m=0;

std::vector<float> *truth_R4_pt=0;
std::vector<float> *truth_R4_eta=0;
std::vector<float> *truth_R4_phi=0;
std::vector<float> *truth_R4_m=0;

std::vector<float> *track_R4_pt=0;
std::vector<float> *track_R4_eta=0;
std::vector<float> *track_R4_phi=0;
std::vector<float> *track_R4_m=0;
//----------------------------------------------------------------------------------
tree->SetBranchAddress("NPV", &npv);
tree->SetBranchAddress("mu_average", &mu);
tree->SetBranchAddress("EventWeight", &evtw);

tree->SetBranchAddress("RecoJets_R4_pt", &reco_R4_pt);
tree->SetBranchAddress("RecoJets_R4_jvf", &reco_R4_jvf);
tree->SetBranchAddress("RecoJets_R4_eta", &reco_R4_eta);
tree->SetBranchAddress("RecoJets_R4_phi", &reco_R4_phi);
tree->SetBranchAddress("RecoJets_R4_m", &reco_R4_m);

tree->SetBranchAddress("TruthJets_R4_pt", &truth_R4_pt);
tree->SetBranchAddress("TruthJets_R4_eta", &truth_R4_eta);
tree->SetBranchAddress("TruthJets_R4_phi", &truth_R4_phi);
tree->SetBranchAddress("TruthJets_R4_m", &truth_R4_m);

tree->SetBranchAddress("TrackJets_R4_pt", &track_R4_pt);
tree->SetBranchAddress("TrackJets_R4_eta", &track_R4_eta);
tree->SetBranchAddress("TrackJets_R4_phi", &track_R4_phi);
tree->SetBranchAddress("TrackJets_R4_m", &track_R4_m);
//----------------------------------------------------------------------------------
TH1F *hist_DR_reco_truth_wc = new TH1F("Delta R reco (with cut)","Delta R; #Delta R; Events",20,0,2);
TH1F *hist_DR_reco_truth_woc = new TH1F("Delta R reco (without cut)","Delta R; #Delta R; Events",20,0,2);
TH1F *hist_DR_track_truth = new TH1F("Delta R track","Delta R; #Delta R; Events",20,0,2);

//----------------------------------------------------------------------------------
int nentries=tree->GetEntries();
std::cout<<nentries<<std::endl;
for (Long64_t ievt=0; ievt<nentries; ievt++)
{
if((ievt%1000)==0){std::cout<<ievt<<std::endl;}
tree->GetEntry(ievt);
    	if(truth_R4_pt->size()>0 && truth_R4_pt->at(0)>20000.)
	{TLorentzVector truthJet;
	 truthJet.SetPtEtaPhiM(truth_R4_pt->at(0),truth_R4_eta->at(0),truth_R4_phi->at(0),truth_R4_m->at(0));        
		if(reco_R4_pt->size()>0)
		{
		TLorentzVector recoJet;
		recoJet.SetPtEtaPhiM(reco_R4_pt->at(0),reco_R4_eta->at(0),reco_R4_phi->at(0),reco_R4_m->at(0)); 
		hist_DR_reco_truth_woc->Fill(truthJet.DeltaR(recoJet),evtw);
		if(std::abs(reco_R4_jvf->at(0))>0.5){hist_DR_reco_truth_wc->Fill(truthJet.DeltaR(recoJet),evtw);}
       		}
		if(track_R4_pt->size()>0)
		{
		TLorentzVector trackJet;
		trackJet.SetPtEtaPhiM(track_R4_pt->at(0),track_R4_eta->at(0),track_R4_phi->at(0),track_R4_m->at(0)); 
		hist_DR_track_truth->Fill(truthJet.DeltaR(trackJet),evtw);
       		}
    	}
}

//----------------------------------------------------------------------------------
auto legend1 = new TLegend(0.55,0.30,0.90,0.50);
//legend1->SetBorderSize(0);
legend1->SetTextSize(0.03);
legend1->AddEntry(hist_DR_track_truth,"#Delta R truth");
legend1->AddEntry(hist_DR_reco_truth_woc,"#Delta R reco (without cut)");
legend1->AddEntry(hist_DR_reco_truth_wc,"#Delta R reco (with cut)");


//----------------------------------------------------------------------------------
TCanvas *canvas6 = new TCanvas("file6","file6",1600,900);
hist_DR_track_truth->Scale(1/hist_DR_track_truth->Integral());
hist_DR_track_truth->SetStats(0);
hist_DR_track_truth->SetMarkerStyle(20);
hist_DR_track_truth->SetMarkerColor(kRed);
hist_DR_track_truth->DrawNormalized("");
hist_DR_reco_truth_woc->Scale(1/hist_DR_reco_truth_woc->Integral());
hist_DR_reco_truth_woc->SetStats(0);
hist_DR_reco_truth_woc->SetMarkerStyle(20);
hist_DR_reco_truth_woc->DrawNormalized("same");
hist_DR_reco_truth_wc->Scale(1/hist_DR_reco_truth_wc->Integral());
hist_DR_reco_truth_wc->SetStats(0);
hist_DR_reco_truth_wc->SetMarkerStyle(20);
hist_DR_reco_truth_wc->SetMarkerColor(kGreen);
hist_DR_reco_truth_wc->DrawNormalized("same");
legend1->Draw("same");
canvas6->Print("problema6.png");
canvas6->Clear();
//------
}
