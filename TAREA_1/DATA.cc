#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>

using namespace RooFit;
using namespace RooStats;


void DATA() 
{

TFile *file = TFile::Open("../Tracks_Clusters.root");
TTree *tree = (TTree*) file->Get("JetRecoTree");
//tree->Print();
//----------------------------------------------------------------------------------
UInt_t npv = -1;
Float_t mu_average = -1;

std::vector<float> *clusters_pt=0, *clusters_phi=0, *clusters_m=0, *clusters_eta=0;
std::vector<float> *tracks_pt=0, *tracks_phi=0, *tracks_m=0, *tracks_eta=0;
std::vector<float> *particles_pt=0, *particles_phi=0, *particles_m=0, *particles_eta=0;
std::vector<int> *tracks_vtx=0, *particles_pdgid=0;
//----------------------------------------------------------------------------------
tree->SetBranchAddress("NPV", &npv);
tree->SetBranchAddress("mu_average", &mu_average);
//----------
tree->SetBranchAddress("Tracks_pt", &tracks_pt);
tree->SetBranchAddress("Tracks_phi", &tracks_phi);
tree->SetBranchAddress("Tracks_m", &tracks_m);
tree->SetBranchAddress("Tracks_eta", &tracks_eta);
//----------
tree->SetBranchAddress("Clusters_pt", &clusters_pt);
tree->SetBranchAddress("Clusters_phi", &clusters_phi);
tree->SetBranchAddress("Clusters_m", &clusters_m);
tree->SetBranchAddress("Clusters_eta", &clusters_eta);
//----------
tree->SetBranchAddress("Particles_pt", &particles_pt);
tree->SetBranchAddress("Particles_phi", &particles_phi);
tree->SetBranchAddress("Particles_m", &particles_m);
tree->SetBranchAddress("Particles_eta", &particles_eta);
//----------
tree->SetBranchAddress("Particles_pdgID", &particles_pdgid);
tree->SetBranchAddress("Tracks_vtx", &tracks_vtx);
//----------------------------------------------------------------------------------
TH1F *hist_npv = new TH1F("NPV","Example plot: Number of primary vertices; NPV ; Events ",50,1,50);
TH1F *hist_mu_average = new TH1F("mu_average","Number of average interactions per bunch-crossing; mu_average",50,0,85);
TH2F *hist2D_npvmu = new TH2F("NPV vs mu_average","NPV vs mu_average",50,1,50,50,0,85);
TH2F *hist2D_npvntracks = new TH2F("NPV vs nTracks","NPV vs nTracks",50,1,50,50,0,1500);
TH2F *hist2D_npvnClusters = new TH2F("NPV vs nClusters","NPV vs nClusters",50,1,50,50,0,1500);
TH2F *hist2D_muntracks = new TH2F("mu_average vs nTracks","mu_average vs nTracks",50,0,85,50,0,1500);
TH2F *hist2D_munClusters = new TH2F("mu_average vs nClusters","mu_average vs nClusters",50,0,85,50,0,1500);
//-----
TH1F *hist_tracks_pt = new TH1F("Tracks_pt","Tracks_pt",50,0,10000);
TH1I *hist_tracks_vtx= new TH1I("Tracks_vtx","Tracks_vtx",40,0,40);
TH1F *hist_tracks_phi = new TH1F("Tracks_phi","Tracks_phi",50,-3.5,3.5);
TH1F *hist_tracks_m = new TH1F("Tracks_m","Tracks_m",50,139,140);
TH1F *hist_tracks_eta = new TH1F("Tracks_eta","Tracks_eta",50,-3,3);
//-----
TH1F *hist_clusters_pt = new TH1F("Clusters_pt","Clusters_pt",50,0,10000);
TH1F *hist_clusters_phi = new TH1F("Clusters_phi","Clusters_phi",50,-3.5,3.5);
TH1F *hist_clusters_m = new TH1F("Clusters_m","Clusters_m",50,-1,1);
TH1F *hist_clusters_eta = new TH1F("Clusters_eta","Clusters_eta",50,-5,5);
//-----
TH1F *hist_particles_pt = new TH1F("Particles_pt","Particles_pt",50,0,10000);
TH1I *hist_particles_pdgID = new TH1I("Particles_pdgID","Particles_pdgID",7000,-3500,3500);
TH1F *hist_particles_phi = new TH1F("Particles_phi","Particles_phi",50,-3.5,3.5);
TH1F *hist_particles_m = new TH1F("Particles_m","Particles_m",180,0,1800);
TH1F *hist_particles_eta = new TH1F("Particles_eta","Particles_eta",50,-6,6);
//----------------------------------------------------------------------------------
int nentries=tree->GetEntries();
std::cout<<nentries<<std::endl;
for (Long64_t ievt=0; ievt<nentries; ievt++)
{
if((ievt%1000)==0){std::cout<<ievt<<std::endl;}
	tree->GetEntry(ievt);
	hist_mu_average->Fill(mu_average);
	hist2D_npvmu->Fill(npv,mu_average);
	hist2D_npvntracks->Fill(npv,tracks_pt->size());
	hist2D_npvnClusters->Fill(npv,clusters_pt->size());
	hist2D_muntracks->Fill(mu_average,tracks_pt->size());
	hist2D_munClusters->Fill(mu_average,clusters_pt->size());

    	for(int tr=0; tr<tracks_pt->size(); tr++)
    	{
        	hist_tracks_pt->Fill(tracks_pt->at(tr));
        	hist_tracks_phi->Fill(tracks_phi->at(tr));
        	hist_tracks_m->Fill(tracks_m->at(tr));
        	hist_tracks_eta->Fill(tracks_eta->at(tr));
        	hist_tracks_vtx->Fill(tracks_vtx->at(tr));
    	}

    	for(int tr=0; tr<clusters_pt->size(); tr++)
    	{
        	hist_clusters_pt ->Fill(clusters_pt->at(tr));
        	hist_clusters_phi->Fill(clusters_phi->at(tr));
        	hist_clusters_m->Fill(clusters_m->at(tr));
        	hist_clusters_eta->Fill(clusters_eta->at(tr));
    	}

    	for(int tr=0; tr<particles_pt->size(); tr++)
    	{
        	hist_particles_pt->Fill(particles_pt->at(tr));
        	hist_particles_pdgID->Fill(particles_pdgid->at(tr));
        	hist_particles_phi->Fill(particles_phi->at(tr));
        	hist_particles_m->Fill(particles_m->at(tr));
        	hist_particles_eta->Fill(particles_eta->at(tr));
    	}

}

//----------------------------------------------------------------------------------
TCanvas *canvas1 = new TCanvas("file1","file1",1600,900);
hist_mu_average->Draw();
canvas1->Print("problema1.png");
canvas1->Clear();
//------
TCanvas *canvas2 = new TCanvas("file2","file2",1600,900);
hist2D_npvmu->Draw();
canvas2->Print("problema2.png");
canvas2->Clear();
//------
TCanvas *canvas3 = new TCanvas("file3","file3",1600,900);
canvas3->Divide(2,2);
canvas3->cd(1);
hist2D_npvntracks->Draw();
canvas3->cd(2);
hist2D_npvnClusters->Draw();
canvas3->cd(3);
hist2D_muntracks->Draw();
canvas3->cd(4);
hist2D_munClusters->Draw();
canvas3->Print("problema3.png");
canvas3->Clear();
//------
TCanvas *canvas41 = new TCanvas("file41","file41",1600,1350);
canvas41->Divide(2,3);
canvas41->cd(1);
hist_tracks_pt->Draw();
canvas41->cd(2);
hist_tracks_phi->Draw();
canvas41->cd(3);
hist_tracks_m->Draw();
canvas41->cd(4);
hist_tracks_eta->Draw();
canvas41->cd(5);
hist_tracks_vtx->Draw();
canvas41->Print("problema41.png");
canvas41->Clear();
//------
TCanvas *canvas42 = new TCanvas("file42","file42",1600,900);
canvas42->Divide(2,2);
canvas42->cd(1);
hist_clusters_pt->Draw();
canvas42->cd(2);
hist_clusters_phi->Draw();
canvas42->cd(3);
hist_clusters_m->Draw();
canvas42->cd(4);
hist_clusters_eta->Draw();
canvas42->Print("problema42.png");
canvas42->Clear();
//------
TCanvas *canvas43 = new TCanvas("file43","file43",1600,1350);
canvas43->Divide(2,3);
canvas43->cd(1);
hist_particles_pt->Draw();
canvas43->cd(2);
hist_particles_pdgID->Draw();
canvas43->cd(3);
hist_particles_phi->Draw();
canvas43->cd(4);
hist_particles_m->Draw();
canvas43->cd(5);
hist_particles_eta->Draw();
canvas43->Print("problema43.png");
canvas43->Clear();
//------

}
